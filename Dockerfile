
ARG ARCH="x86_64"
ARG BASE_VERSION=latest
ARG ZIG_VERSION="0.13.0"
ARG ZIG_FOLDERNAME=zig-linux-${ARCH}-${ZIG_VERSION}
ARG ZIG_FILENAME=${ZIG_FOLDERNAME}.tar.xz
ARG ZIG_URL="https://ziglang.org/download/${ZIG_VERSION}/${ZIG_FILENAME}"

FROM docker.io/alpine:${BASE_VERSION} AS base

FROM base AS download-zig

ARG ZIG_URL
ARG ZIG_FILENAME

RUN mkdir -p /zig
WORKDIR /zig
ADD "${ZIG_URL}" .
RUN tar -xf "${ZIG_FILENAME}"

FROM scratch AS artifacts

COPY --from=download-zig "/zig/${ZIG_FOLDERNAME}" "/zig/${ZIG_FOLDERNAME}"

FROM base AS zig

ARG ZIG_FOLDERNAME

COPY --from=download-zig "/zig/${ZIG_FOLDERNAME}" "/var/lib/${ZIG_FOLDERNAME}"
COPY zig-cc-env.sh /bin/zig-cc-env.sh

RUN ln -s "/var/lib/${ZIG_FOLDERNAME}/zig" /bin/zig && \
    printf '#!/usr/bin/env sh\n\nzig cc "$@"\n' > /bin/zig-cc && \
    printf '#!/usr/bin/env sh\n\nzig c++ "$@"\n' > /bin/zig-c++ && \
    chmod +x /bin/zig-* && \
    ls /bin/zig* && \
    zig version

FROM zig AS kcov-build

RUN apk add --no-cache cmake ninja python3 \
    musl-dev binutils-dev curl-dev elfutils-dev

WORKDIR /root

ADD https://github.com/SimonKagstrom/kcov/archive/refs/tags/v43.tar.gz .
RUN mkdir -p kcov/src kcov/build kcov/install && \
    tar x -C kcov/src --strip-components 1 -f v43.tar.gz

WORKDIR /root/kcov/build

ARG KCOV_TARGET="x86_64-linux-musl"
ENV CC="zig-cc"
ENV CFLAGS="-isystem /usr/include -L /lib -L /usr/lib -lc -lm --target=${KCOV_TARGET}"
ENV CXX="zig-c++"
ENV CXXFLAGS="${CFLAGS} -D__ptrace_request=int"

RUN cmake -G Ninja -DCMAKE_INSTALL_PREFIX=/root/kcov/install /root/kcov/src && \
    cmake --build . --target install
# RUN find /root/kcov/install && false

FROM zig AS kcov

RUN apk add --no-cache binutils libcurl libelf libdw git
COPY --from=kcov-build /root/kcov/install/bin/kcov /bin/kcov
COPY --from=kcov-build /root/kcov/install/bin/kcov-system-daemon /bin/kcov-system-daemon

# FROM zig AS final