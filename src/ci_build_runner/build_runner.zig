const build_runner_builtin = @import("builtin");
const build_runner_import = switch (build_runner_builtin.zig_version.minor) {
    13 => @import("0.13.0/build_runner.zig"),
    14 => @import("master/build_runner.zig"),
    else => @compileError("Your Zig version is not supported: " ++ build_runner_builtin.zig_version_string),
};
pub const dependencies = build_runner_import.dependencies;

pub fn main() !void {
    return build_runner_import.main();
}