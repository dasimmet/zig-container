const std = @import("std");

pub fn print_deps(dependencies: type) !void {
    const stdout_writer = std.io.getStdOut().writer();

    const depdecls = comptime std.meta.declarations(dependencies.packages);
    inline for (depdecls) |dep| {
        const decl = @field(dependencies.packages, dep.name);
        if (@hasDecl(decl, "available") and decl.available) {
            try stdout_writer.print("dep: {s}, {s}\n", .{dep.name, decl.build_root});
        } else {
            try stdout_writer.print("missing dep: {s}\n", .{dep.name});
        }
    }
    for (dependencies.root_deps) |dep| {
        try stdout_writer.print("root_dep: {s} {s}\n", .{dep[0],dep[1]});
    }
}
