#!/usr/bin/env sh

export CC="zig-cc"
export CXX="zig-c++"
export CFLAGS="-lc -lm $CFLAGS"
export CXXFLAGS="-lc -lm $CXXFLAGS"

[ -n "$*" ] && "$@"