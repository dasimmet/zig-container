# Zig Container

a basic alpine:latest based container image including [zig](https://ziglang.org/)
and [kcov](https://docs.kernel.org/dev-tools/kcov.html) from [kcov-git](https://github.com/SimonKagstrom/kcov)
for collecting coverage reports.
image tags are:
- latest=zig master
- snapshots in the format: 0.12.0-dev.1245-a07f288eb
- releases: 0.11.0, 0.12.0, 0.13.0

the latest version is updated regularily, and older snapshots are cleaned up on schedule.

```console
docker run -it --rm registry.gitlab.com/dasimmet/zig-container:latest zig zen

 * Communicate intent precisely.
 * Edge cases matter.
 * Favor reading code over writing code.
 * Only one obvious way to do things.
 * Runtime crashes are better than bugs.
 * Compile errors are better than runtime crashes.
 * Incremental improvements.
 * Avoid local maximums.
 * Reduce the amount one must remember.
 * Focus on code rather than style.
 * Resource allocation may fail; resource deallocation must succeed.
 * Memory is a resource.
 * Together we serve the users.

```